/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author ecaam
 */
public class Refrigeradores {
    String marca;
    String modelo;
    int    annofabricacion;
    
    // Crando Métodos estáticos
    static void imprimirTexto(String texto){
        System.out.println(texto);
    }
    static void imprimirEntero(int entero){
        System.out.println(entero);
    }
    
    static void imprimirObjeto(Refrigeradores myObjeto){
        imprimirTexto(myObjeto.marca);
        imprimirTexto(myObjeto.modelo);
        imprimirEntero(myObjeto.annofabricacion);
        imprimirTexto("-----------------------------------");
        
    }
    
    // Métos públicos 
    public void sinEscarcha(){
        System.out.println("No genera Escarcha");
    }
    public void conEscarcha(){
        System.out.println("Genera Escarcha, requiere limieza");
    }
    
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Refrigeradores mabe = new Refrigeradores();
        mabe.marca           = "Mabe";
        mabe.modelo          = "Automática";
        mabe.annofabricacion = 2019;
        
        Electrodomesticos refrigerador = new Electrodomesticos();
        
        refrigerador.tipo            = "Refrigerador";
        refrigerador.annofabricacion = 2020;
        refrigerador.paisdeOrigen    = "China";
        
        Electrodomesticos lavadora   = new Electrodomesticos();
        lavadora.tipo                = "Lavadora";
        lavadora.annofabricacion     = 2020;
        lavadora.paisdeOrigen        = "México";
        
         imprimirTexto(refrigerador.tipo);
         imprimirEntero(refrigerador.annofabricacion);
         imprimirTexto(refrigerador.paisdeOrigen);
         imprimirTexto(lavadora.tipo);
         imprimirEntero(lavadora.annofabricacion);
         imprimirTexto(lavadora.paisdeOrigen);
         
        
        imprimirObjeto(mabe);
        mabe.sinEscarcha();
        
        Refrigeradores lg = new Refrigeradores();
        
        lg.marca           = "LG";
        lg.modelo          = "Semiautomática";
        lg.annofabricacion = 2020;
        
        imprimirObjeto(lg);
        lg.conEscarcha();
        
        
        Refrigeradores daewoo = new Refrigeradores();
        daewoo.marca           = "Daewoo";
        daewoo.modelo          = "Automático sin escarcha";
        daewoo.annofabricacion = 2021;
        imprimirObjeto(daewoo);
        daewoo.sinEscarcha();
      
    }
    
}
