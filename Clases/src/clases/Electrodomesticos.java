/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author ecaam
 */
public class Electrodomesticos {
       private String tipo;
       private String paisdeOrigen;
       private int    annofabricacion;

    public Electrodomesticos() {
    }

    public Electrodomesticos(String tipo, String paisdeOrigen, int annofabricacion) {
        this.tipo = tipo;
        this.paisdeOrigen = paisdeOrigen;
        this.annofabricacion = annofabricacion;
    }

    public String getTipo() {
        return tipo;
    }

    public String getPaisdeOrigen() {
        return paisdeOrigen;
    }

    public int getAnnofabricacion() {
        return annofabricacion;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setPaisdeOrigen(String paisdeOrigen) {
        this.paisdeOrigen = paisdeOrigen;
    }

    public void setAnnofabricacion(int annofabricacion) {
        this.annofabricacion = annofabricacion;
    }
           
    
}
